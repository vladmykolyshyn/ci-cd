import { getTimestamp } from '../src/utils';

describe('Test getTimestamp function', () => {
  it('should return a valid ISO-8601 timestamp', () => {
    const timestamp = getTimestamp();
    const regex = /^\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}.\d{3}Z$/;
    expect(timestamp).toMatch(regex);
  });
});