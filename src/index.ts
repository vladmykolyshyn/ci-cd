import dotenv from 'dotenv';
import express, { Express, Request, Response } from 'express';
import { getTimestamp } from './utils';

dotenv.config();

const app: Express = express();
const port = process.env.PORT;

app.get('/', (req: Request, res: Response) => {
  res.send(`Express + TypeScript Server, Timestamp: ${getTimestamp()}`);
});

app.listen(port, () => {
  console.log(`Server running on http://localhost:${port}`);
});

